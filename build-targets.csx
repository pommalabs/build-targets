// Dependencies must be synchronized with "build-targets.nuspec" file.
#r "nuget: Bullseye, 4.2.1"
#r "nuget: CommandLineParser, 2.9.1"
#r "nuget: SimpleExec, 11.0.0"

using System.Xml.Linq;
using System.Xml.XPath;
using CommandLine;
using static Bullseye.Targets;
using static SimpleExec.Command;

////////////////////////////////////////////////////////////////////////////////
// PREPARATION
////////////////////////////////////////////////////////////////////////////////

var artifactsDir = "./artifacts";
var nugetPackagesDir = $"{artifactsDir}/nuget-packages";
var testResultsDir = $"{artifactsDir}/test-results";
var codeCoverageDir = $"{artifactsDir}/code-coverage";

////////////////////////////////////////////////////////////////////////////////
// OPTIONS
////////////////////////////////////////////////////////////////////////////////

public sealed partial class Options
{
    [Option('c', "configuration", Required = false, Default = "release")]
    public string Configuration { get; set; }

    public string ConfigurationFlag => !string.IsNullOrWhiteSpace(Configuration) ? $"--configuration \"{Configuration}\"" : string.Empty;

    [Option('f', "framework", Required = false, Default = "")]
    public string Framework { get; set; }

    public string FrameworkFlag => !string.IsNullOrWhiteSpace(Framework) ? $"--framework \"{Framework}\"" : string.Empty;

    [Option('s', "skip-dependencies", Required = false, Default = false)]
    public bool SkipDependencies { get; set; }

    [Option('t', "target", Required = false, Default = "run-tests")]
    public string Target { get; set; }

    [Option("targets", Required = false)]
    public IEnumerable<string> Targets { get; set; }

    [Option("auto-increment", Required = false, Default = false)]
    public bool AutoIncrement { get; set; }

    [Option("sonar-flags", Required = false, Default = "")]
    public string SonarFlags { get; set; }

    [Option("sonar-host-url", Required = false, Default = "https://sonarcloud.io")]
    public string SonarHostUrl { get; set; }

    [Option("sonar-organization", Required = false, Default = "")]
    public string SonarOrganization { get; set; }

    [Option("sonar-project", Required = false, Default = "")]
    public string SonarProject { get; set; }

    [Option("sonar-token", Required = false, Default = "")]
    public string SonarToken { get; set; }
}

Options opts;

string GetOption(string inputOptionValue, string environmentVariableName, bool required)
{
    var optionValue = string.IsNullOrWhiteSpace(inputOptionValue)
        ? Environment.GetEnvironmentVariable(environmentVariableName)
        : inputOptionValue;

    return (string.IsNullOrWhiteSpace(optionValue) && required)
        ? throw new ArgumentException($"{environmentVariableName} option is missing but it is required")
        : optionValue;
}

////////////////////////////////////////////////////////////////////////////////
// TARGETS - BUILD
////////////////////////////////////////////////////////////////////////////////

void UpdateVersion(string version)
{
    Console.WriteLine(version);
    var srcBuildProps = "./src/Directory.Build.props";
    var srcBuildPropsDoc = XDocument.Parse(File.ReadAllText(srcBuildProps));
    var versionElem = srcBuildPropsDoc.XPathSelectElement("/Project/PropertyGroup/Version");
    versionElem.SetValue(version);
    File.WriteAllText(srcBuildProps, $"{srcBuildPropsDoc.Declaration}{Environment.NewLine}{srcBuildPropsDoc}");
}

Target("git-version", async () =>
{
    var parts = (await ReadAsync("git", "describe --tags")).StandardOutput.Trim().Split("-").Take(4).ToArray();
    var commitSha = (await ReadAsync("git", "rev-parse --short=8 HEAD")).StandardOutput.Trim();
    string versionNumber, version;
    if (opts.AutoIncrement)
    {
        // Handled cases:
        // x            -> 1 part, tag is applied to current commit.
        // x-height-sha -> 3 parts, tag has been applied on a previous commit.
        versionNumber = parts[0];
        var gitHeight = parts.Length > 1 ? parts[1] : "0";
        version = $"{versionNumber}.{gitHeight}+{commitSha}";
    }
    else
    {
        // Handled cases:
        // x.y.z                -> 1 part, tag is applied to current commit.
        // x.y.z-pre            -> 2 parts, pre-release tag is applied to current commit.
        // x.y.z-height-sha     -> 3 parts, tag has been applied on a previous commit.
        // x.y.z-pre-height-sha -> 4 parts, pre-release tag has been applied on a previous commit.
        versionNumber = (parts.Length % 2 == 1) ? parts[0] : string.Join("-", parts.Take(2));
        version = $"{versionNumber}+{commitSha}";
    }
    UpdateVersion(version);
});

Target("check-source-code-style", async () =>
{
    await RunAsync("dotnet", "format --verbosity normal --verify-no-changes");
});

Target("clean-solution", async () =>
{
    await RunAsync("dotnet", $"clean {opts.ConfigurationFlag}");
    if (Directory.Exists(artifactsDir)) Directory.Delete(artifactsDir, recursive: true);
});

Target("restore-nuget-packages", DependsOn("clean-solution"), async () =>
{
    await RunAsync("dotnet", $"restore");
});

Target("build-solution", DependsOn("restore-nuget-packages"), async () =>
{
    await RunAsync("dotnet", $"build {opts.ConfigurationFlag} --no-restore");
});

Target("run-tests", DependsOn("build-solution"), async () =>
{
    var loggerFlag = "--logger \"junit;LogFileName={assembly}-{framework}.xml;MethodFormat=Class;FailureBodyFormat=Verbose\"";
    var codeCoverageFlags = $"--settings \"coverlet.runsettings\" --results-directory \"{testResultsDir}\"";
    await RunAsync("dotnet", $"test {opts.ConfigurationFlag} {opts.FrameworkFlag} --test-adapter-path . {loggerFlag} {codeCoverageFlags} --no-build");
});

////////////////////////////////////////////////////////////////////////////////
// TARGETS - NUGET
////////////////////////////////////////////////////////////////////////////////

Target("pack-sources", DependsOn("build-solution"), async () =>
{
    await RunAsync("dotnet", $"pack {opts.ConfigurationFlag} --output \"{nugetPackagesDir}\" --no-build");
});

////////////////////////////////////////////////////////////////////////////////
// TARGETS - CODE COVERAGE
////////////////////////////////////////////////////////////////////////////////

Target("generate-coverage-report", DependsOn("run-tests"), async () =>
{
    await RunAsync("dotnet", $"reportgenerator -reports:\"{testResultsDir}/*/*.xml\" -targetdir:\"{codeCoverageDir}\" -reporttypes:\"Html\"");
});

////////////////////////////////////////////////////////////////////////////////
// TARGETS - SONAR SCANNER
////////////////////////////////////////////////////////////////////////////////

Target("activate-sonar-scanner", DependsOn("clean-solution"), async () =>
{
    var sonarFlags = GetOption(opts.SonarFlags, "SONAR_FLAGS", required: false);
    var sonarHostUrl = GetOption(opts.SonarHostUrl, "SONAR_HOST_URL", required: true);
    var sonarOrganization = GetOption(opts.SonarOrganization, "SONAR_ORGANIZATION", required: true);
    var sonarProject = GetOption(opts.SonarProject, "SONAR_PROJECT", required: true);
    var sonarToken = GetOption(opts.SonarToken, "SONAR_TOKEN", required: true);
    var codeCoverageFlag = $"/d:sonar.cs.opencover.reportsPaths=\"{testResultsDir}/**/coverage.opencover.xml\"";
    await RunAsync("dotnet", $"sonarscanner begin /k:\"{sonarProject}\" /o:\"{sonarOrganization}\" /d:sonar.host.url=\"{sonarHostUrl}\" /d:sonar.login=\"{sonarToken}\" {codeCoverageFlag} {sonarFlags}");
});

Target("publish-sonar-report", DependsOn("activate-sonar-scanner", "run-tests"), async () =>
{
    var sonarToken = GetOption(opts.SonarToken, "SONAR_TOKEN", required: true);
    await RunAsync("dotnet", $"sonarscanner end /d:sonar.token=\"{sonarToken}\"");
});

////////////////////////////////////////////////////////////////////////////////
// EXECUTION
////////////////////////////////////////////////////////////////////////////////

async Task Build(IList<string> args)
{
    var parsed = Parser.Default.ParseArguments<Options>(args);
    await parsed.MapResult(
        async o =>
        {
            opts = o;
            var targets = opts.Targets?.ToArray() ?? Array.Empty<string>();
            targets = (targets.Length > 0) ? targets : new[] { opts.Target };
            await RunTargetsAndExitAsync(targets, new Bullseye.Options { SkipDependencies = opts.SkipDependencies });
        },
        errors => Task.CompletedTask
    );
}
